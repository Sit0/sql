create user 'keeper' identified by 'keeper1';
create database Zoo;
grant all on Zoo.* to 'keeper';

use Zoo;
create table animals (
name varchar(256),
species_name varchar(256),
age decimal(2)
);
create table breeders (
name varchar(256),
surname varchar(256)
);

insert into animals 
(name, age, species_name) 
values 
('Feesrir', 3,'wolf'), 
('Zrizard', 1, 'lynx'), 
('Makha', 1, 'wolf'), 
('Kuzil', 4, 'lynx'), 
('Limriast', 6, 'tiger'), 
('Dhaka', 2, 'tiger');

insert into breeders 
(name, surname)
values
('Dmitri', 'Winogradoff'),
('Kazimir', 'Litvinov'),
('Ranka', 'Litvinskii');

select * from breeders;
select count(*) from animals;
select * from animals where age < 3;
select * from animals where species_name ='wolf';
select * from animals where age > 2 and species_name ='lynx';
select * from animals where species_name ='tiger';

alter table breeders add primary key (name,surname);
alter table animals add column (breeder_name varchar(256),breeder_surname varchar(256));
alter table animals add foreign key (breeder_name,breeder_surname) references breeders (name,surname);

update animals set breeder_name = 'Dmitri' where species_name = 'wolf';
update animals set breeder_name = 'Kazimir' where species_name = 'lynx';
update animals set breeder_name = 'Ranka' where species_name = 'tiger';

update animals set breeder_surname = (select surname from breeders where animals.breeder_name = name);

select name from animals where breeder_name = 'Dmitri' and breeder_surname = 'Winogradoff';
select count(*) from animals where breeder_name = 'Kazimir' and breeder_surname = 'Litvinov';

select count(*) from animals where breeder_name = 'Kazimir' and breeder_surname = 'Litvinov';

create table species (name varchar(256));
alter table species add primary key (name);

alter table animals add column species;
alter table animals add foreign key (species) references species (name);

insert into species (name) select distinct species_name from animals;
//!dlaczego nie insert into species (name) values = (select distinct species_name from animals)

update animals set species = (select name from species where species.name=animals.species_name);
lub update animals set species = species_name;

alter table animals drop column species_name;

alter table animals add column gender enum('male','female');

update animals 

update animals SET gender = 'female' where name like '%a';
update animals SET gender = 'male' where name not like '%a';

alter table breeders add column position enum('junior','middle','senior');

update breeders set position = 'middle';

select name,gender from animals where breeder_name = 'Ranka' and breeder_surname = 'Litvinskii';

insert into animals 
(name, age, species, gender)
values
('Ivan', 7, 'lynx','male'),
('Tatianka', 4, 'wolf', 'female'),
('Balbina', 3, 'tiger', 'female');

insert into animals 
(name, age, species, gender)
values
('Ivan', 7, 'lynx','male'),
('Tatianka', 4, 'wolf', 'female'),
('Balbina', 3, 'tiger', 'female');

insert into breeders 
(name, surname, position)
values
('Hannes', 'Runge', 'middle'),
('Johenn', 'Sholl', 'middle'),
('Margit', 'Laskowski', 'middle'),
('Janik', 'Eckstein', 'middle'),
('Klaudia', 'Linn', 'middle');




















